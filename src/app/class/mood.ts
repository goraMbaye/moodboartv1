export class Mood {
  constructor(
    public direction: string,
    public departement: string,
    public content: number,
    public neutre: number,
    public mecontent: number,
    public date: string,
    public commentaire?: string
  ) {  }
}
