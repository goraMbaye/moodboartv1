import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  show = '';

  constructor(private router: Router) { }

  ngOnInit() {
    if (!localStorage.getItem('direction') || localStorage.getItem('direction') === ''){
      this.router.navigate([ '/login' ]);
    }

    if (!localStorage.getItem('tuto') || localStorage.getItem('tuto') !== '1'){
      setTimeout(() => {
        this.show = 'md-show';
      }, 1000);
    }

  }

  tuto(){
    this.show = '';
    localStorage.setItem('tuto', '1');
  }
}
