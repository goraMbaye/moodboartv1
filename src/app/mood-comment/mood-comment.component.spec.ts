import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoodCommentComponent } from './mood-comment.component';

describe('MoodCommentComponent', () => {
  let component: MoodCommentComponent;
  let fixture: ComponentFixture<MoodCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoodCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoodCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
