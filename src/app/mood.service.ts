import { Mood } from './class/mood';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

const reqOpts = {
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: ''
  },
  params: new HttpParams()
};

const api = "https://mood-back.herokuapp.com/";

@Injectable({
  providedIn: 'root'
})
export class MoodService {

  constructor(private http: HttpClient) { }

  addMood (mood: Mood) {
    return this.http.post(api + 'mood', mood, httpOptions);
  }

  getMoodDirection(dir){
    return this.http.post(api + 'get_dir_mood', dir, httpOptions);
  }

  getDirections () {
    return this.http.get(api + 'get_direction');
  }

  getDartement(dir){
    return this.http.post(api + 'get_dep', dir, httpOptions);
  }
}
